/****** Object:  Table [dbo].[staffs]    Script Date: 12/23/2021 4:47:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[staffs](
	[staff_id] [int] IDENTITY(1,1) NOT NULL,
	[first_name] [varchar](50) NOT NULL,
	[last_name] [varchar](50) NOT NULL,
	[email] [varchar](255) NOT NULL,
	[phone] [varchar](25) NULL,
	[active] [tinyint] NOT NULL,
	[store_id] [int] NOT NULL,
	[manager_id] [int] NULL,
	[Rating] [int] NULL,
	[Bonus] [decimal](20, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[staff_id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[staffs] ON 
GO
INSERT [dbo].[staffs] ([staff_id], [first_name], [last_name], [email], [phone], [active], [store_id], [manager_id], [Rating], [Bonus]) VALUES (1, N'Fabiola', N'Jackson', N'fabiola.jackson@bikes.shop', N'(831) 555-5554', 1, 1, NULL, NULL, NULL)
GO
INSERT [dbo].[staffs] ([staff_id], [first_name], [last_name], [email], [phone], [active], [store_id], [manager_id], [Rating], [Bonus]) VALUES (2, N'Mireya', N'Copeland', N'mireya.copeland@bikes.shop', N'(831) 555-5555', 1, 1, 1, NULL, NULL)
GO
INSERT [dbo].[staffs] ([staff_id], [first_name], [last_name], [email], [phone], [active], [store_id], [manager_id], [Rating], [Bonus]) VALUES (3, N'Genna', N'Serrano', N'genna.serrano@bikes.shop', N'(831) 555-5556', 1, 1, 2, NULL, NULL)
GO
INSERT [dbo].[staffs] ([staff_id], [first_name], [last_name], [email], [phone], [active], [store_id], [manager_id], [Rating], [Bonus]) VALUES (4, N'Virgie', N'Wiggins', N'virgie.wiggins@bikes.shop', N'(831) 555-5557', 1, 1, 2, NULL, NULL)
GO
INSERT [dbo].[staffs] ([staff_id], [first_name], [last_name], [email], [phone], [active], [store_id], [manager_id], [Rating], [Bonus]) VALUES (5, N'Jannette', N'David', N'jannette.david@bikes.shop', N'(516) 379-4444', 1, 2, 1, NULL, NULL)
GO
INSERT [dbo].[staffs] ([staff_id], [first_name], [last_name], [email], [phone], [active], [store_id], [manager_id], [Rating], [Bonus]) VALUES (6, N'Marcelene', N'Boyer', N'marcelene.boyer@bikes.shop', N'(516) 379-4445', 1, 2, 5, NULL, NULL)
GO
INSERT [dbo].[staffs] ([staff_id], [first_name], [last_name], [email], [phone], [active], [store_id], [manager_id], [Rating], [Bonus]) VALUES (7, N'Venita', N'Daniel', N'venita.daniel@bikes.shop', N'(516) 379-4446', 1, 2, 5, NULL, NULL)
GO
INSERT [dbo].[staffs] ([staff_id], [first_name], [last_name], [email], [phone], [active], [store_id], [manager_id], [Rating], [Bonus]) VALUES (8, N'Kali', N'Vargas', N'kali.vargas@bikes.shop', N'(972) 530-5555', 1, 3, 1, NULL, NULL)
GO
INSERT [dbo].[staffs] ([staff_id], [first_name], [last_name], [email], [phone], [active], [store_id], [manager_id], [Rating], [Bonus]) VALUES (9, N'Layla', N'Terrell', N'layla.terrell@bikes.shop', N'(972) 530-5556', 1, 3, 7, NULL, NULL)
GO
INSERT [dbo].[staffs] ([staff_id], [first_name], [last_name], [email], [phone], [active], [store_id], [manager_id], [Rating], [Bonus]) VALUES (10, N'Bernardine', N'Houston', N'bernardine.houston@bikes.shop', N'(972) 530-5557', 1, 3, 7, NULL, NULL)
GO
INSERT [dbo].[staffs] ([staff_id], [first_name], [last_name], [email], [phone], [active], [store_id], [manager_id], [Rating], [Bonus]) VALUES (11, N'Matt', N'Fletcher', N'Mathew.Fletcher@bikes.shop', N'(972) 446-7895', 1, 3, 1, NULL, NULL)
GO
INSERT [dbo].[staffs] ([staff_id], [first_name], [last_name], [email], [phone], [active], [store_id], [manager_id], [Rating], [Bonus]) VALUES (12, N'Kerry', N'Parmer', N'Kerry.Parmer@bikes.shop', N'(831) 555-5667', 1, 1, 1, NULL, NULL)
GO
INSERT [dbo].[staffs] ([staff_id], [first_name], [last_name], [email], [phone], [active], [store_id], [manager_id], [Rating], [Bonus]) VALUES (13, N'Havish', N'Maharathy', N'Havish.Maharathy@bikes.shop', N'(516) 379-4456', 1, 2, 1, NULL, NULL)
GO
INSERT [dbo].[staffs] ([staff_id], [first_name], [last_name], [email], [phone], [active], [store_id], [manager_id], [Rating], [Bonus]) VALUES (14, N'Peter', N'Mcguire', N'Peter.Mcguire@bikes.shop', N'(516) 379-7881', 1, 2, 5, NULL, NULL)
GO
INSERT [dbo].[staffs] ([staff_id], [first_name], [last_name], [email], [phone], [active], [store_id], [manager_id], [Rating], [Bonus]) VALUES (15, N'Kaine', N'Saunders', N'Kaine.Saunders@bikes.shop', N'(972) 530-1912', 1, 3, 7, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[staffs] OFF
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__staffs__AB6E6164C2AC9C9B]    Script Date: 12/23/2021 4:47:29 PM ******/
ALTER TABLE [dbo].[staffs] ADD UNIQUE NONCLUSTERED 
(
	[email] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[staffs] ADD  CONSTRAINT [DEF_Active]  DEFAULT ((1)) FOR [active]
GO
ALTER TABLE [dbo].[staffs]  WITH CHECK ADD FOREIGN KEY([manager_id])
REFERENCES [dbo].[staffs] ([staff_id])
GO
